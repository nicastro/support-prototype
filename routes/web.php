<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'tenancy.enforce'], function () {
    Auth::routes();

    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('layout', function () {
        return view('child');
    });//

    Route::resource('customer', 'CustomerController');
    Route::resource('company', 'CompanyController');
    Route::resource('ticket', 'TicketController');
    Route::resource('tag', 'TagController');

});


//Route::get('/customer', function () {
//  return view('customer');
//});

