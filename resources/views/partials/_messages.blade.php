@if (Session::has('company_store_success'))
    <div class="message success">
        {{ Session::get('company_store_success') }}
    </div>
@endif

@if (Session::has('customer_store_success'))
    <div class="message success">
        {{ Session::get('customer_store_success') }}
    </div>
@endif


@if (count($errors) > 0)
    <div class="message error">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>
@endif
