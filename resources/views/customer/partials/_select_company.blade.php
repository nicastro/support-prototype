{{--<label for="company" class="hidden">Company</label>--}}
<select name="company">
    <option value="" disabled>Choose</option>
    @foreach($companies as $company)
        @if($company->id == $customer->company_id)
            <option value="{{$company->id}}" selected="selected">{{$company->name}}</option>
        @else
            <option value="{{$company->id}}">{{$company->name}}</option>
        @endif
    @endforeach
</select>
