@extends('layouts.layout')
@section('title', 'Customers')
@section('content')



    <div class="row customer-view">

        <div class="col-12 col-sm-4 col-md-3 mb-4">
            <div class="card mb-4">
                <div class="card-body">
                    <!-- <h5 class="mb-4">Form Grid</h5>-->

                    <div style="text-align: center" class="*">
                        <span style="font-size: 100px;" class="oi oi-person"></span>
                    </div>
                    <div class="row">
                        <div style="text-align: center" class=" col-12 value-group">
                            @if($customer->company && $customer->company->name)
                                <a href="/company/{{$customer->company->id}}" class="truncate">{{$customer->company->name}}</a>
                            @else
                                <span class="truncate">-</span>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-12 col-sm-8 col-md-8 col-lg-6  mb-4">
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="mb-4">Form Grid</h5>

                    <!--                        <form>-->
                    <div class="row">
                        <div class="col-6 value-group">
                            <div class="">
                                <span>Anrede</span>
                                <span>{{$customer->salutation}}</span>
                            </div>
                        </div>
                        <div class="col-6 value-group">

                            <div class="">
                                <span>Titel</span>
                                <span>{{$customer->additional_title}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6 value-group">
                            <div class="">
                                <span>Vorname</span>
                                <span>{{$customer->firstname}}</span>
                            </div>
                        </div>
                        <div class="col-6 value-group">
                            <div class="">
                                <span>Nachname</span>
                                <span>{{$customer->lastname}}</span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12 value-group">
                            <div class="">
                                <span>Email</span>
                                <span>{{$customer->email}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6 value-group">
                            <div class="">
                                <span>Telefon</span>
                                <span>{{$customer->telephone}}</span>
                            </div>
                        </div>

                        <div class="col-6 value-group">
                            <div class="">
                                <span>Mobile</span>
                                <span>{{$customer->mobile}}</span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12 value-group">
                            <div class="">
                                <span>Funktion</span>
                                <span>{{$customer->function}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 value-group">
                            <div class="">
                                <span>Abteilung/Gebäude/Stockwerk</span>
                                @if($customer->department_building_floor)
                                    <span>{!! nl2br(e($company->department_building_floor)) !!}</span>
                                @else
                                    <span>-</span>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">1</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                        <label class="form-check-label" for="inlineCheckbox2">2</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"
                               disabled>
                        <label class="form-check-label" for="inlineCheckbox3">3 (disabled)</label>
                    </div>
                    <a href="{{ route('customer.edit', $customer->id) }}">
                        <button type="submit" class="btn btn-primary d-block mt-3">Bearbeiten</button>
                    </a>
                </div>
            </div>
        </div>

    </div>
@endsection


@section('scripts')

    <script>
        $(document).ready(function () {

        });
    </script>
@endsection