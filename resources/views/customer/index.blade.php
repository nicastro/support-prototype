@extends('layouts.layout')
@section('title', 'Customers')
@section('content')



    <div class="row customer-list">
        <div class="col-12" data-check-all="checkAll">

            @foreach($customers as $customer)
                <div class="card d-flex flex-row mb-3">
                    <a href="{{ route('customer.show', $customer->id) }}">
                        <div class="d-flex flex-grow-1 min-width-zero">
                            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">

                                <div class="firstname-lastname list-item-heading mb-1 w-25 w-xs-100">
                                    <img src="img/dani.jpg" alt="">
                                    <div>
                                        <span class="firstname">{{ $customer->firstname }}</span>&nbsp;<span class="lastname">{{ $customer->lastname }}</span>
                                    </div>
                                </div>

                                <div class="contact mb-1 truncate w-25 w-xs-100">
                                    @if($customer->email)
                                        <span class="email">{{ $customer->email }}</span>
                                    @endif
                                    @if($customer->telephone)
                                        <span class="telephone">{{ $customer->telephone }}</span>
                                    @endif
                                    @if($customer->mobile)
                                        <span class="mobile">{{ $customer->mobile }}</span>
                                    @endif
                                </div>

                                <div class="function mb-1 w-25 w-xs-100">
                                    <div class="function-text">
                                        <span class="">Funktion</span>
                                        @if($customer->job_function)
                                            <span>{{ $customer->job_function }}</span>
                                        @else
                                            <span>-</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="company d-flex mb-1 w-15 w-xs-100">
                                    <img class="selection" src="img/dani.jpg" alt="">
                                    <div class="company-text">
                                        <span class="">Firma</span>
                                        @if($customer->company && $customer->company->name)
                                            <span class="truncate">{{$customer->company->name}}</span>
                                        @else
                                            <span class="truncate">-</span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a>
                </div>
            @endforeach


            <nav class="mt-4 mb-3">
                <ul class="pagination justify-content-center mb-0">
                    <li class="page-item ">
                        <a class="page-link first" href="#">
                            <i class="simple-icon-control-start"></i>
                        </a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link prev" href="#">
                            <i class="simple-icon-arrow-left"></i>
                        </a>
                    </li>
                    <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">3</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link next" href="#" aria-label="Next">
                            <i class="simple-icon-arrow-right"></i>
                        </a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link last" href="#">
                            <i class="simple-icon-control-end"></i>
                        </a>
                    </li>
                </ul>
            </nav>

        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $(document).ready(function () {

        });
    </script>
@endsection