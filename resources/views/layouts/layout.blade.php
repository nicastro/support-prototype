<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Support</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!--Import materialize.css-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link type="text/css" rel="stylesheet" href="/css/app.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="/css/styles.css" media="screen,projection"/>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>App Name - @yield('title')</title>


    <!--    <link rel="stylesheet" href="font/iconsmind/style.css"/>-->
    <!--    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css"/>-->
{{--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"--}}
{{--integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--}}
<!--    <link rel="stylesheet" href="css/vendor/perfect-scrollbar.css"/>-->
    <!--    <link rel="stylesheet" href="css/vendor/jquery.contextMenu.min.css"/>-->
{{--<link rel="stylesheet" href="styles/css/styles.css"/>--}}
<!--    <link href="styles/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">-->
    {{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">--}}


</head>

<body class="main-is-hide sub-menu-hidden">
@include('partials._nav')
<main>
    <div class="container-fluid">

        @include('partials._messages')
        @include('partials._top_content')

        @yield('content')
    </div>
</main>
<script src="{{ asset('js/app.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@yield('scripts')

</body>
</html>