@extends('layouts.layout')
@section('title', 'Page Title')
@section('content')


    <div class="row ticket-row">


        <div class="col-12 col-lg-9 ticket-info">
            <div class="card d-flex mb-3">
                <div class="card-body">
                    <div class="row ticket-header">
                        <div class="col-sm-12 col-lg-9 title"><h2>#523 - Filteransicht benötigt Filteransicht</h2></div>
                        <div class="col-sm-12 col-lg-3 date align-self-center text-marking--dumb">am 27.10.2018 - 13.36 Uhr</div>
                    </div>
                    <div class="row ticket-header-receiver border-bottom">
                        <div class="col-sm-6 text-marking--dumb">
                            <table>
                                <tr>
                                    <td>
                                        <div>Von:</div>
                                        <div>CC:</div>
                                    </td>
                                    <td>
                                        <div>myvmy@vymo.com</div>
                                        <div>info@vymo.com</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row ticket-description">
                        <div class="col-sm-12">
                            <p>
                                Sehr geehrter Herr Sowieso
                                <br/> <br/>
                                Gerne möchte ich auf Sie zu kommen, da es nicht ganz klar ist wie und was genau funktioniert.
                                <br/><br/>
                                Haben Sie bereits ein Auge auf die Ansicht für den Shop werden können? Es wäre gut wenn Weit hinten, hinter den Wortbergen, fern der Länder
                                kalien und Konsonantien leben die Blindtexte. Abgeschieden wohnen sie in Buchstabhausen an der Küste des Semantik, eines großen
                                Sprachozeans.
                                <br/><br/>
                                Ein kleines Bächlein namens Duden fließt durch ihren Ort und versorgt sie mit den nötigen Regelialien. Es ist ein paradiesmatisches Land, in
                                dem einem gebratene Satzteile in den Mund fliegen. Nicht einmal von der allmächtigen Interpunktion werden die Blindtexte beherrscht – ein
                                geradezu unorthographisches Leben. Eines Tages aber beschloß eine kleine Zeile Blindtext, ihr Name war Lorem Ipsum, hinaus zu gehen in die
                                weite Grammatik.
                                <br/><br/>
                                Beste Grüsse<br/>
                                Py Valeris Meier
                                <br/><br/>
                                Py Vymo<br/>
                                +487856446<br/>
                                Meierstrasse 99<br/>
                                www.py-vymo.com
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-3 ticket-meta">
            <div class="card d-flex flex-row mb-3">
                <div class="d-flex flex-grow-1 min-width-zero">

                    {{--<form class="card-body" action="">--}}

                    @if(isset($ticket->id))
                        ticket isset
                        {!! Form::model($ticket,['action'=>['TicketController@update',$ticket->id],'class'=>'card-body']) !!}
                        {{ method_field('PATCH') }}

                    @else
                        ticket NOT isset
                        {!! Form::model($ticket,['action'=>'TicketController@store','class'=>'card-body']) !!}
                        {{--{{ method_field('PUT') }}--}}

                    @endif


                    <div class="row agent wrapper">
                        <div class="col-12 d-flex">
                            @include('ticket.partials._select_customer')
                        </div>
                    </div>

                    <div class="row company wrapper">
                        <div class="col-12 d-flex">
                            @include('ticket.partials._show_company')
                        </div>
                    </div>

                    <div class="row border-bottom">
                        <div class="col-12 d-flex">
                        </div>
                    </div>

                    <div class="row customer wrapper">
                        <div class="col-12 d-flex">
                            @include('ticket.partials._select_agent')
                        </div>
                    </div>

                    <div class="row border-bottom wrapper">
                        <div class="col-12 d-flex">
                        </div>
                    </div>

                    <div id="center" class="">

                        <div class="row margin-auto status wrapper">
                            <div class="col-12 d-flex">
                                @include('ticket.partials._select_status')
                            </div>
                        </div>

                        <div class="row margin-auto priority wrapper">
                            <div class="col-12 d-flex">
                                @include('ticket.partials._select_priority')
                            </div>
                        </div>

                        <div class="row margin-auto source wrapper">
                            <div class="col-12 d-flex">
                                @include('ticket.partials._select_source')
                            </div>
                        </div>

                        <div class="row margin-auto type wrapper">
                            <div class="col-12 d-flex">
                                @include('ticket.partials._select_type')
                            </div>
                        </div>

                        <div class="row margin-auto tags wrapper">
                            <div class="col-12 d-flex">
                                @include('ticket.partials._select_tags')
                            </div>
                        </div>
                    </div>

                    <div class="row margin-auto border-bottom ">
                        <div class="col-12 d-flex">
                        </div>
                    </div>

                    <div class="form-row margin-auto row confirmed_effort wrapper">
                        <div class="col-12 form-group">
                            <label for="confirmed_effort" class="text-marking--dumb">Bestätigter Aufwand</label>
                            <input name="confirmed_effort" type="text" class="form-control" id="confirmed_effort">
                        </div>
                    </div>

                    <div class="form-row margin-auto row submit wrapper">
                        <div class="form-group margin-auto col-12">
                            <button type="submit" class="btn btn-primary mt-3">Speichern</button>
                        </div>
                    </div>
                    {!! Form::close() !!}

                    {{--</form>--}}
                </div>
            </div>
        </div>

        <div class="col-12 col-lg-9 col-md-12 ticket-info response">
            <span class="fa fa-envelope"></span>
            <div class="card d-flex mb-3 mt-3">
                <div class="card-body">
                    <div class="row ticket-header border-bottom">
                        <div class="col-6 col-sm-6 col-md-7 col-lg-8 receiver align-self-center text-marking--dumb">
                            <table>
                                <tr>
                                    <td>
                                        <div>An:</div>
                                        <div>BCC:</div>

                                    </td>
                                    <td>
                                        <div>myvmy@vymo.com</div>
                                        <div>info@vymo.com</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-1 type align-self-center">
                            <span class="fa fa-envelope"></span>
                        </div>
                        <div class="col-6 col-sm-5 col-md-4 col-lg-3 agent align-self-center text-marking--dumb">
                            <div class="agent-name"><img src="img/dani.jpg" alt=""> <span>Daniele Nicastro</span></div>
                            <div class="send-date"><span class="prefix">am&nbsp;</span><span>27.10.2018 - 13.36</span><span class="suffix">&nbsp;Uhr</span></div>
                        </div>
                    </div>
                    <div class="row ticket-description">
                        <div class="col-sm-12">
                            <p>
                                Sehr geehrter Herr Sowieso
                                <br/> <br/>
                                Gerne möchte ich auf Sie zu kommen, da es nicht ganz klar ist wie und was genau funktioniert.
                                <br/><br/>
                                Haben Sie bereits ein Auge auf die Ansicht für den Shop werden können? Es wäre gut wenn Weit hinten, hinter den Wortbergen, fern der Länder
                                kalien und Konsonantien leben die Blindtexte. Abgeschieden wohnen sie in Buchstabhausen an der Küste des Semantik, eines großen
                                Sprachozeans.
                                <br/><br/>
                                Beste Grüsse<br/>
                                Py Valeris Meier
                            </p>
                        </div>

                    </div>


                    <div class="row ticket-mail-history">
                        <div class="col-sm-12">

                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                                    aria-controls="collapseTwo">
                                                <span class="fa fa-chevron-right"></span>
                                                <span class="fa fa-chevron-down"></span>
                                                <span>------</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non
                                            cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                            on
                                            it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                            nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
                                            aesthetic
                                            synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="col-12 col-lg-9 col-md-12 ticket-info response">
            <span class="fa fa-phone"></span>
            <div class="card d-flex mb-3 mt-3">
                <div class="card-body">
                    <div class="row ticket-header  border-bottom">
                        <div class="col-6 col-sm-6 col-md-7 col-lg-8 receiver align-self-center text-marking--dumb">
                            <table>
                                <tr>
                                    <td>
                                        <div>An:</div>
                                        <div>BCC:</div>
                                    </td>
                                    <td>
                                        <div>myvmy@vymo.com</div>
                                        <div>info@vymo.com</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-1 type align-self-center">
                            <span class="fa fa-envelope"></span>
                        </div>
                        <div class="col-6 col-sm-5 col-md-4 col-lg-3 agent align-self-center text-marking--dumb">
                            <div class="agent-name"><span class="fa fa-user-md"></span> <span>Daniele Nicastro</span></div>
                            <div class="send-date"><span class="prefix">am&nbsp;</span><span>27.10.2018 - 13.36</span><span class="suffix">&nbsp;Uhr</span></div>
                        </div>
                    </div>
                    <div class="row ticket-description">
                        <div class="col-sm-12">
                            <p>
                                Sehr geehrter Herr Sowieso
                                <br/> <br/>
                                Manchmal Sätze, die alle Buchstaben des Alphabets enthalten – man nennt diese Sätze »Pangrams«. Sehr bekannt ist dieser: The quick brown fox
                                jumps over the lazy old dog. Oft werden in Typoblindtexte auch fremdsprachige Satzteile eingebaut (AVAIL® and Wefox™ are testing aussi la
                                Kerning), um die Wirkung in anderen Sprachen zu testen. In Lateinisch sieht zum Beispiel fast jede Schrift gut aus. Quod erat demonstrandum.
                                <br/><br/>
                                Seit 1975 fehlen in den meisten Testtexten die Zahlen, weswegen nach TypoGb. 204 § ab dem Jahr 2034 Zahlen in 86 der Texte zur Pflicht
                                werden.
                                Nichteinhaltung wird mit bis zu 245 € oder 368 $ bestraft. Genauso wichtig in sind mittlerweile auch Âçcèñtë, die in neueren Schriften aber
                                fast
                                immer enthalten sind. Ein wichtiges aber schwierig zu integrierendes Feld sind OpenType-Funktionalitäten. Je nach Software und
                                Voreinstellungen
                                können eingebaute Kapitälchen, Kerning oder Ligaturen (sehr pfiffig) nicht richtig dargestellt werden.
                                <br/><br/>
                                Beste Grüsse<br/>
                                Py Valeris Meier
                            </p>
                        </div>
                    </div>
                    <div class="row ticket-mail-history">
                        <div class="col-sm-12">

                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                                    aria-controls="collapseTwo">
                                                <span class="fa fa-chevron-right"></span>
                                                <span class="fa fa-chevron-down"></span>
                                                <span>---</span>
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="collapseThree" data-parent="#accordion">
                                        <div class="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non
                                            cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird
                                            on
                                            it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                            nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
                                            aesthetic
                                            synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row ticket-attachments">
                        <div class="col-12 files-list">

                            <div class="file-attachment border">
                                <a href="?open">
                                    <span class="fa fa-file-pdf"></span>
                                    <span class="filename">tickets-pdf</span>
                                </a>
                                <div class="action">
                                    <a href="?download" class="download"><span class="fa fa-download"></span>
                                        <span class="filesize text-marking--dumb text-extra-small">1.1 MB</span>
                                    </a>
                                </div>
                            </div>

                            <div class="file-attachment border">
                                <a href="?open">
                                    <span class="fa fa-image"></span>
                                    <span class="filename">screensh otscree.png</span>
                                </a>
                                <div class="action">
                                    <a href="?download" class="download"><span class="fa fa-download"></span>
                                        <span class="filesize text-marking--dumb text-extra-small">1.1 MB</span>
                                    </a>
                                </div>
                            </div>

                            <div class="file-attachment border">
                                <a href="?open">
                                    <span class="fa fa-file"></span>
                                    <span class="filename">Beschreibung.docx</span>
                                </a>
                                <div class="action">
                                    <a href="?download" class="download"><span class="fa fa-download"></span>
                                        <span class="filesize text-marking--dumb text-extra-small">1.1 MB</span>
                                    </a>
                                </div>
                            </div>


                            <div class="file-attachment border">
                                <a href="?open">
                                    <span class="fa fa-file"></span>
                                    <span class="filename">Keine Info.docx</span>
                                </a>
                                <div class="action">
                                    <a href="?download" class="download"><span class="fa fa-download"></span>
                                        <span class="filesize text-marking--dumb text-extra-small">1.1 MB</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')

    <script>
        $(document).ready(function () {
            $('select.with-img').select2({
                theme: "bootstrap",
                templateResult: formatStateImg,
            });

            $('select.with-icon').select2({
                theme: "bootstrap",
                templateResult: formatResultIcon,
                templateSelection: formatSelectionIcon,
            });

            $('select.tags').select2({
                width: "100%",
                // maximumSelectionSize: 6,

                // templateResult: formatResultIcon,
                // templateSelection: formatSelectionIcon,
            });

            $('select.normal').select2({
                theme: "bootstrap",

            });

            $('select.with-img').on('select2:select', function (e) {
                $(this).parent().prev().attr('src', 'img/' + $(this).val() + '.jpg');
            });

            function formatStateImg(state) {
                if (!state.id)
                    return state.text;

                var baseUrl = "./img";
                return $(
                    '<span><img src="' + baseUrl + '/' + state.element.value + '.jpg" class="img-flag" />' + state.text + '</span>'
                );
            };

            function formatResultIcon(state) {
                if (!state.id)
                    return state.text;

                var icon = state.element.value;
                if (state.element.value === 'email') {
                    icon = 'envelope';
                }
                if (state.element.value === 'from_customer') {
                    icon = 'user-circle';
                }
                if (state.element.value === 'extension') {
                    icon = 'puzzle-piece';
                }
                return $(
                    '<div class="format-selection-icon--wrapper"><span class="fa fa-' + icon + '"></span><span>' + state.text + '</span></div>'
                );
                // return $state;
            };

            function formatSelectionIcon(state) {
                if (!state.id)
                    return state.text;

                var icon = state.element.value;
                if (state.element.value === 'email') {
                    icon = 'envelope';
                }
                if (state.element.value === 'from_customer') {
                    icon = 'user-circle';
                }

                if (state.element.value === 'extension') {
                    icon = 'puzzle-piece';
                }
                return $(
                    '<div class="format-result-icon--wrapper"><span class="fa fa-' + icon + '"></span><span>' + state.text + '</span></div>'
                );
            };
        });
    </script>
@endsection