<div class="label-select-wrapper d-flex">
    <label for="status" class="text-marking--dumb">Status</label>
    <select class="selection-item normal" name="status" id="status">
        <option value="" disabled selected>Choose</option>
        @foreach($statuses as $status)
            @if($status->id == $ticket->status_id)
                <option value="{{$status->id}}" selected="selected">{{$status->label}}</option>
            @else
                <option value="{{$status->id}}">{{$status->label}}</option>
            @endif
        @endforeach
    </select>
    <span class="fa fa-chevron-down"><i class="hidden">status</i></span>
</div>