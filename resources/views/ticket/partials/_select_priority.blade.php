<div class="label-select-wrapper d-flex">
    <label for="priority" class="text-marking--dumb">Priorität</label>
    <select class="selection-item normal" name="priority" id="priority">
        <option value="" disabled selected>Choose</option>
        @foreach($priorities as $priority)
            @if($priority->id == $ticket->priority_id)
                <option value="{{$priority->id}}" selected="selected">{{$priority->label}}</option>
            @else
                <option value="{{$priority->id}}">{{$priority->label}}</option>
            @endif
        @endforeach
    </select>
    <span class="fa fa-chevron-down"><i class="hidden">priority</i></span>
</div>

{{--<div class="label-select-wrapper d-flex">--}}
{{--<label for="priority" class="text-marking--dumb">Priorität</label>--}}
{{--<select class="selection-item normal" name="priority" id="priority">--}}
{{--<option value="low">Tief</option>--}}
{{--<option value="normal">Normal</option>--}}
{{--<option value="contract">Vertrag</option>--}}
{{--<option style="color:red" value="high">Dringend</option>--}}
{{--</select>--}}
{{--<span class="fa fa-chevron-down"><i class="hidden">priority</i></span>--}}
{{--</div>--}}