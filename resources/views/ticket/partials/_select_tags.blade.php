<div class="label-select-wrapper d-flex">
    {{Form::label('tags','Tags',['class'=>'text-marking--dumb'])}}
    {{Form::select('tags[]',$tags,$tags_stored,['class'=>'tags','multiple'=>'multiple'])}}
</div>


{{--<div class="label-select-wrapper d-flex">--}}
{{--<label for="tags" class="text-marking--dumb">Tags</label>--}}
{{--<select class="tags" name="tags[]" id="tags" multiple="multiple">--}}
{{--<option value="question">Frage</option>--}}
{{--<option value="bug">Problem</option>--}}
{{--<option value="extension">Erweiterung</option>--}}
{{--</select>--}}
{{--</div>--}}