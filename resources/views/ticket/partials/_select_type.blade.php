<div class="label-select-wrapper d-flex">
    <label for="status" class="text-marking--dumb">Typ</label>
    <select class="selection-item normal" name="type" id="type">
        <option value="" disabled selected>Choose</option>
        @foreach($types as $type)
            @if($type->id == $ticket->type_id)
                <option value="{{$type->id}}" selected="selected">{{$type->label}}</option>
            @else
                <option value="{{$type->id}}">{{$type->label}}</option>
            @endif
        @endforeach
    </select>
    <span class="fa fa-chevron-down"><i class="hidden">type</i></span>
</div>


{{--<div class="label-select-wrapper d-flex">--}}
    {{--<label for="source" class="text-marking--dumb">Quelle</label>--}}
    {{--<select class="selection-item with-icon" name="type" id="type">--}}
        {{--<option value="question">Frage</option>--}}
        {{--<option value="bug">Problem</option>--}}
        {{--<option value="extension">Erweiterung</option>--}}
    {{--</select>--}}
    {{--<span class="fa fa-chevron-down"><i class="hidden">priority</i></span>--}}
{{--</div>--}}