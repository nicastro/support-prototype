<img class="selection" src="img/dani.jpg" alt="">
<div class="label-select-wrapper d-flex">
    <label for="agent" class="text-marking--dumb">Agent</label>
    <select class="with-img selection-item" name="agent" id="agent">
        <option value="" disabled selected>Choose</option>
        @foreach($agents as $agent)
            @if($agent->id == $ticket->agent_id)
                <option value="{{$agent->id}}" selected="selected">{{$agent->firstname}}&nbsp;{{$agent->lastname}}</option>
            @else
                <option value="{{$agent->id}}">{{$agent->firstname}}&nbsp;{{$agent->lastname}}</option>
            @endif
        @endforeach
    </select>
    <span class="fa fa-chevron-down"><i class="hidden">agent</i></span>
</div>

{{--<img class="selection" src="img/dani.jpg" alt="">--}}
{{--<div class="label-select-wrapper d-flex">--}}
{{--<label for="agent" class="text-marking--dumb">Agent</label>--}}
{{--<select class="with-img selection-item" name="agent" id="agent">--}}
{{--<option value="dani">Daniele Nicastro</option>--}}
{{--<option value="nora">Nora Meier</option>--}}
{{--<option value="maya">Maya Meier</option>--}}
{{--<option value="hans">Marc Neugass</option>--}}
{{--</select>--}}
{{--<span class="fa fa-chevron-down"><i class="hidden">agent</i></span>--}}
{{--</div>--}}