<div class="label-select-wrapper d-flex">
    <label for="source" class="text-marking--dumb">Quelle</label>
    <select class="selection-item with-icon" name="source" id="source">
        <option value="" disabled selected>Choose</option>
        @foreach($sources as $source)
            @if($source->id == $ticket->source_id)
                <option value="{{$source->id}}" selected="selected">{{$source->label}}</option>
            @else
                <option value="{{$source->id}}">{{$source->label}}</option>
            @endif
        @endforeach
    </select>
    <span class="fa fa-chevron-down"><i class="hidden">Quelle</i></span>
</div>

{{--<div class="label-select-wrapper d-flex">--}}
{{--<label for="source" class="text-marking--dumb">Quelle</label>--}}
{{--<select class="selection-item with-icon" name="source" id="source">--}}
{{--<option value="email">E-Mail</option>--}}
{{--<option value="phone">Telefon</option>--}}
{{--<option value="from_customer">Von Kunde erfasst</option>--}}
{{--</select>--}}
{{--<span class="fa fa-chevron-down"><i class="hidden">priority</i></span>--}}
{{--</div>--}}