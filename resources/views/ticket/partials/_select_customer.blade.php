<img class="selection" src="img/dani.jpg" alt="">
<div class="label-select-wrapper d-flex">
    <label for="customer" class="text-marking--dumb">Kunde</label>
    <select class="with-img selection-item" name="customer" id="customer">
        <option value="" disabled selected>Choose</option>
        @foreach($customers as $customer)
            @if($customer->id == $ticket->customer_id)
                <option value="{{$customer->id}}" selected="selected">{{$customer->firstname}}&nbsp;{{$customer->lastname}}</option>
            @else
                <option value="{{$customer->id}}">{{$customer->firstname}}&nbsp;{{$customer->lastname}}</option>
            @endif
        @endforeach
    </select>
    <span class="fa fa-chevron-down"><i class="hidden">customer</i></span>
</div>

{{--<img class="selection" src="img/dani.jpg" alt="">--}}
{{--<div class="label-select-wrapper d-flex">--}}
{{--<label for="customer" class="text-marking--dumb">Kunde</label>--}}
{{--<select class="with-img selection-item" name="customer" id="customer">--}}
{{--<option value="dani">Daniele Nicastro</option>--}}
{{--<option value="nora">Nora Meier</option>--}}
{{--<option value="maya">Maya Meier</option>--}}
{{--<option value="hans">Marc Neugass</option>--}}
{{--</select>--}}
{{--<span class="fa fa-chevron-down"><i class="hidden">customer</i></span>--}}
{{--</div>--}}