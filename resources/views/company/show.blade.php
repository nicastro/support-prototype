@extends('layouts.layout')
@section('title', 'Company')
@section('content')



    <div class="row company-view">

        <div class="col-12 col-sm-4 col-md-3 mb-4">
            <div class="card mb-4">
                <div class="card-body">
                    <!-- <h5 class="mb-4">Form Grid</h5>-->

                    <div style="text-align: center" class="*">
                        <span style="font-size: 100px;" class="oi oi-person"></span>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-12 col-sm-8 col-md-8 col-lg-6  mb-4">
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="mb-4">Form Grid</h5>

                    <!--                        <form>-->
                    <div class="row">
                        <div class="col-12 value-group">
                            <div class="">
                                <span>Firmenname</span>
                                <span>{{ $company->name }} </span>
                            </div>
                        </div>
                        <div class="col-12 value-group">

                            <div class="">
                                <span>Email</span>
                                @if($company->email)
                                    <span class="email">{{ $company->email }}</span>
                                @else
                                    <span>-</span>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-6 value-group">
                            <div class="">
                                <span>Telefon</span>
                                @if($company->telephone)
                                    <span class="telephone">{{ $company->telephone }} </span>
                                @else
                                    <span>-</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-6 value-group">
                            <div class="">
                                <span>Mobile</span>
                                @if($company->mobile)
                                    <span class="mobile">{{ $company->mobile }}</span>
                                @else
                                    <span>-</span>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12 value-group">
                            <div class="">
                                <span>Website</span>
                                @if($company->website)
                                    <span><a href="{{ $company->website }}">{{ $company->website }}</a></span>
                                @else
                                    <span>-</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 value-group">
                            <div class="">
                                <span>Adresse</span>
                                @if($company->address)
                                    <span>{!! nl2br(e($company->address)) !!}</span>
                                @else
                                    <span>-</span>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12 value-group">
                            <div class="">
                                <span>Zusätzliche Informationen</span>
                                @if($company->additional_info)
                                    <span>{!! nl2br(e($company->additional_info)) !!}</span>
                                @else
                                    <span>-</span>
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">1</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                        <label class="form-check-label" for="inlineCheckbox2">2</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"
                               disabled>
                        <label class="form-check-label" for="inlineCheckbox3">3 (disabled)</label>
                    </div>
                    <a href="{{ route('company.edit', $company->id) }}">
                        <button type="submit" class="btn btn-primary d-block mt-3">Bearbeiten</button>
                    </a>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('scripts')

    <script>
        $(document).ready(function () {

        });
    </script>
@endsection