@extends('layouts.layout')
@section('title', 'companies')
@section('content')


    <div class="row company-list">
        <div class="col-12" data-check-all="checkAll">
            @foreach($companies as $company)


                <div class="card d-flex flex-row mb-3">
                    <a href="{{ route('company.show', $company->id) }}">
                        <div class="d-flex flex-grow-1 min-width-zero">
                            <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">

                                <div class="name list-item-heading mb-1 w-30 w-xs-100">
                                    <img src="img/dani.jpg" alt="">
                                    <div>
                                        <span class="name">{{ $company->name }}</span>
                                    </div>
                                </div>

                                <div class="contact mb-1 truncate w-25 w-xs-100">
                                    @if($company->email)
                                        <span class="email">{{ $company->email }}</span>
                                    @endif
                                    @if($company->telephone)
                                        <span class="telephone">{{ $company->telephone }} </span>
                                    @endif
                                    @if($company->mobile)
                                        <span class="mobile">{{ $company->mobile }}</span>
                                    @endif
                                </div>

                                <div class="website mb-1 w-25 w-xs-100">
                                    <div class="website-text">
                                        <span>Website</span>
                                        @if($company->website)
                                            <span>{{$company->website}}</span>
                                        @else
                                            <span>-</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="address mb-1 truncate w-15 w-xs-100">
                                    @if($company->address)
                                        <span>{!! nl2br(e($company->address)) !!}</span>
                                    @else
                                        <span>-</span>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </a>
                </div>
            @endforeach

            <nav class="mt-4 mb-3">
                <ul class="pagination justify-content-center mb-0">
                    <li class="page-item ">
                        <a class="page-link first" href="#">
                            <i class="simple-icon-control-start"></i>
                        </a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link prev" href="#">
                            <i class="simple-icon-arrow-left"></i>
                        </a>
                    </li>
                    <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">3</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link next" href="#" aria-label="Next">
                            <i class="simple-icon-arrow-right"></i>
                        </a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link last" href="#">
                            <i class="simple-icon-control-end"></i>
                        </a>
                    </li>
                </ul>
            </nav>

        </div>
    </div>


@endsection

@section('scripts')

    <script>
        $(document).ready(function () {

        });
    </script>
@endsection