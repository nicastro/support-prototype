@extends('layouts.layout')
@section('title', 'Page Title')
@section('content')


    <div class="row">

        <div class="col-12 col-xl-4 mb-4">
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="mb-4">Form Grid</h5>

                    {!! Form::model($company,['action'=>'CompanyController@store','class'=>'']) !!}
                    <div style="text-align: center" class="form-group col-md-12">
                        <span style="font-size: 100px;" class="oi oi-person"></span>
                    </div>


                    <div style="text-align: center" class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary mt-3">Hide this Button</button>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>


        <div class="col-12 col-xl-8 mb-4">
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="mb-4">Form Grid</h5>

                    {!! Form::model($company,['action'=>'CompanyController@store','class'=>'']) !!}

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            {{ Form::label('name', 'Firmenname') }}
                            {{ Form::text('name','', ['class'=>'validate form-control']) }}
                        </div>
                        <div class="form-group col-md-12">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::text('email','', ['class'=>'validate form-control']) }}

                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::email('email','', ['class'=>'validate form-control']) }}

                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-6">
                            {{ Form::label('telephone', 'Telefon') }}
                            {{ Form::tel('telephone','', ['class'=>'validate form-control']) }}

                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('mobile', 'Mobile') }}
                            {{ Form::tel('mobile','', ['class'=>'validate form-control']) }}
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            {{ Form::label('website', 'Website') }}
                            {{ Form::text('website','', ['class'=>'validate form-control']) }}

                        </div>
                    </div>


                    <div class="form-group">
                        {{ Form::label('address', 'Addresse') }}
                        {{ Form::textarea('address','',['id'=>'address','class'=>'form-control','data-length'=>'600']) }}

                    </div>

                    <div class="form-group">
                        {{ Form::label('additional_info', 'Zusätzliche Informationen') }}
                        {{ Form::textarea('additional_info','',['id'=>'additional_info','class'=>'form-control','data-length'=>'600']) }}
                    </div>


                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                        <label class="form-check-label" for="inlineCheckbox1">1</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                        <label class="form-check-label" for="inlineCheckbox2">2</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"
                               disabled>
                        <label class="form-check-label" for="inlineCheckbox3">3 (disabled)</label>
                    </div>

                    {{ Form::button('Speichern',['value'=>'Submit','type'=>'submit','name'=>'action','class'=>'btn btn-primary d-block mt-3']) }}


                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('textarea#address, textarea#additional_info').characterCounter();
        });


    </script>
@endsection