<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');

            $table->string('subject')->nullable();

            $table->integer('customer_id')->nullable()->unsigned();
            $table->integer('agent_id')->nullable()->unsigned();

            $table->integer('status_id')->nullable()->unsigned();
            $table->integer('priority_id')->nullable()->unsigned();
            $table->integer('source_id')->nullable()->unsigned();
            $table->integer('type_id')->nullable()->unsigned();


            $table->string('confirmed_effort')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
