<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Source extends Model
{
    use  UsesTenantConnection;

    public function ticket()
    {
        return $this->hasMany(Ticket::class);
    }

}
