<?php

namespace App;

use Hyn\Tenancy\Traits\UsesTenantConnection;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{

    use  UsesTenantConnection;


    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }

    public function source()
    {
        return $this->belongsTo(Source::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }
}
