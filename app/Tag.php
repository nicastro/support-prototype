<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Tag extends Model
{
    use  UsesTenantConnection;

    public function tickets()
    {
        return $this->belongsToMany('App\Ticket');
    }
}
