<?php

/*
 * This file is part of the hyn/multi-tenant package.
 *
 * (c) Daniël Klabbers <daniel@klabbers.email>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://laravel-tenancy.com
 * @see https://github.com/hyn/multi-tenant
 */

namespace App;

use Hyn\Tenancy\Contracts\Website\UuidGenerator;
use Hyn\Tenancy\Contracts\Website;
use Hyn\Tenancy\Generators\Uuid\ShaGenerator;
use Ramsey\Uuid\Uuid;

class TenantIdGenerator implements UuidGenerator
{

    /**
     * @param Website $website
     *
     * @return string
     */
    public function generate(Website $website): string
    {
//        $uuid = Uuid::uuid4()->toString();
//
//        if (config('tenancy.website.uuid-limit-length-to-32')) {
//            return str_replace('-', null, $uuid);
//        }

        return $website->hostnames;
    }
}
