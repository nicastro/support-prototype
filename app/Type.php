<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Type extends Model
{
    use  UsesTenantConnection;
    public function ticket()
    {
        return $this->hasMany(Ticket::class);
    }
}
