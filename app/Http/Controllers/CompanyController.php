<?php

namespace App\Http\Controllers;

use App\Company;
use App\Customer;
use App\Http\Requests\CompanyStoreRequest;
use Illuminate\Http\Request;
use Session;

class CompanyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('company.index', ['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = new Company();
        return view('company.create', ['company' => $company]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyStoreRequest $request)
    {
        if ($request->validated()) {
            $company                  = new Company();
            $company->name            = $request->name;
            $company->email           = $request->email;
            $company->telephone       = $request->telephone;
            $company->mobile          = $request->mobile;
            $company->website         = $request->website;
            $company->address         = $request->address;
            $company->additional_info = $request->additional_info;

            $company->save();

            Session::flash('company_store_success', 'Firma wurde erfolgreich hinzugefügt');

            return view('company.show', ['company' => $company]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company $company
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $customer = Customer::all()->sortByDesc('name');
        return view('company.show', [
          'customer' => $customer,
          'company'  => $company,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company $company
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('company.edit', ['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Company             $company
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyStoreRequest $request, Company $company)
    {
        if ($request->validated()) {
            $company->name            = $request->name;
            $company->email           = $request->email;
            $company->telephone       = $request->telephone;
            $company->mobile          = $request->mobile;
            $company->website         = $request->website;
            $company->address         = $request->address;
            $company->additional_info = $request->additional_info;

            $company->save();

            Session::flash('company_store_success', 'Kunde erfolgreich aktualisiert');

            return view('company.show', ['company' => $company]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company $company
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete(); // @todo: Maybe Soft deleting?

        Session::flash('company_store_success', 'Firma wurde erfolgreich gelöscht.');

        return redirect()->route('company.index');
    }
}
