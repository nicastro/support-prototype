<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\CustomerStoreRequest;
use Illuminate\Http\Request;
use App\Customer;
use Session;


class CustomerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view('customer.index', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer  = new Customer();
        $companies = Company::all()->sortByDesc('name');
        return view('customer.create', [
          'customer'  => $customer,
          'companies' => $companies,
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CustomerStoreRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(CustomerStoreRequest $request)
    {
        if ($request->validated()) {
            $customer                            = new Customer();
            $customer->firstname                 = $request->firstname;
            $customer->lastname                  = $request->lastname;
            $customer->email                     = $request->email;
            $customer->telephone                 = $request->telephone;
            $customer->mobile                    = $request->mobile;
            $customer->job_function              = $request->job_function;
            $customer->department_building_floor = $request->department_building_floor;
            $customer->company_id                = $request->company;

            $customer->save();

            Session::flash('customer_store_success', 'Kunde wurde erfolgreich hinzugefügt');

            return view('customer.show', ['customer' => $customer]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        $company = Company::all()->sortByDesc('name');
        return view('customer.show', [
          'customer' => $customer,
          'company'  => $company,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $companies = Company::all()->sortByDesc('name');

        return view('customer.edit', [
          'customer'  => $customer,
          'companies' => $companies,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Customer            $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerStoreRequest $request, Customer $customer)
    {
        if ($request->validated()) {
            $customer->firstname                 = $request->firstname;
            $customer->lastname                  = $request->lastname;
            $customer->email                     = $request->email;
            $customer->telephone                 = $request->telephone;
            $customer->mobile                    = $request->mobile;
            $customer->job_function              = $request->job_function;
            $customer->department_building_floor = $request->department_building_floor;
            $customer->company_id                = $request->company;

            $customer->save();

            Session::flash('customer_store_success', 'Kunde erfolgreich aktualisiert');

            return view('customer.show', ['customer' => $customer]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer $customer
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete(); // @todo: Maybe Soft deleting?

        Session::flash('customer_store_success', 'Kunde wurde erfolgreich gelöscht.');

        return redirect()->route('customer.index');
    }
}
