<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Customer;
use App\Priority;
use App\Status;
use App\Tag;
use App\Ticket;
use App\Source as TicketSource;

use App\Type;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Source;

class TicketController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers  = Customer::all();
        $agents     = Agent::all()->sortByDesc('name');
        $statuses   = Status::all();
        $priorities = Priority::all();
        $sources    = TicketSource::all();
        $types      = Type::all();
        $tags       = Tag::all();

        return view('ticket.show', [
          'ticket'     => new Ticket(),
          'customers'  => $customers,
          'agents'     => $agents,
          'tags'       => $tags,
          'statuses'   => $statuses,
          'priorities' => $priorities,
          'sources'    => $sources,
          'types'      => $types,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = new Ticket();
        $tag    = Tag::find(1);

        isset($request->status) ? $ticket->status()->associate($request->status) : null;
        isset($request->priority) ? $ticket->priority()->associate($request->priority) : null;
        isset($request->source) ? $ticket->source()->associate($request->source) : null;
        isset($request->type) ? $ticket->type()->associate($request->type) : null;

        $ticket->save();

        return redirect(route('ticket.show', ['ticket' => $ticket]));


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket $ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        $customers  = Customer::all();
        $agents     = Agent::all()->sortByDesc('name');
        $statuses   = Status::all();
        $priorities = Priority::all();
        $sources    = TicketSource::all();
        $types      = Type::all();

        // We can process this better, but we want go further :)
        $tags_available = [];
        foreach (Tag::all() as $tag) {
            $tags_available[$tag->id] = $tag->label;
        }
        $tags_stored = [];
        if ($ticket->tags()) {
            foreach ($ticket->tags()->allRelatedIds() as $ticket_tag) {
                $tags_stored[$ticket_tag] = $ticket_tag;
            }
        }

        return view('ticket.show', [
          'ticket'      => $ticket,
          'customers'   => $customers,
          'agents'      => $agents,
          'tags'        => $tags_available,
          'tags_stored' => $tags_stored,
          'statuses'    => $statuses,
          'priorities'  => $priorities,
          'sources'     => $sources,
          'types'       => $types,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket $ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        return redirect(route('ticket.show', ['ticket' => $ticket]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Ticket              $ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        isset($request->status) ? $ticket->status()->associate($request->status) : null;
        isset($request->priority) ? $ticket->priority()->associate($request->priority) : null;
        isset($request->source) ? $ticket->source()->associate($request->source) : null;
        isset($request->type) ? $ticket->type()->associate($request->type) : null;

        $tags = isset($request->tags) ? array_values($request->tags) : [];
        $ticket->tags()->sync($tags);

        $ticket->save();

        return redirect(route('ticket.show', ['ticket' => $ticket]));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket $ticket
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        $ticket->tags()->detach();
        $ticket->delete();

        return view('ticket.index');
    }
}
