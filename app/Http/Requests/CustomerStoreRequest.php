<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerStoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // @todo: Maybe change needed after implementing User-Auth
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': // Create
                return [
                  'firstname'  => 'required|max:255',
                  'lastname'   => 'nullable|max:255',
                  'email'      => 'required|max:255|email|unique:customers,email',
                  'telephone'  => 'nullable|max:255|unique:customers,telephone',
                  'mobile'     => 'nullable|max:255|unique:customers,mobile',
                  'company_id' => 'nullable|integer',
                ];

            case 'PUT':
            case 'PATCH': // Update
                $customer_id = $this->customer->id;
                return [
                  'firstname'  => 'required|max:255',
                  'lastname'   => 'nullable|max:255',
                  'email'      => "required|max:255|email|unique:customers,email,$customer_id",
                  'telephone'  => "nullable|max:255|unique:customers,telephone,$customer_id",
                  'mobile'     => "nullable|max:255|unique:customers,mobile,$customer_id",
                  'company_id' => 'nullable|integer',

                ];
        }
    }

    public function messages()
    {
        return [
          'email.required' => 'Email erforderlich!',
          'firstname.max'  => 'zuviel Zeichen!',
        ];
    }
}
