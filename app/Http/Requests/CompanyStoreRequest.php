<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyStoreRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': // Create
                return [
                  'name'      => 'required|max:255',
                  'email'     => 'nullable|max:255|email|unique:companies,email',
                  'telephone' => 'nullable|max:255|unique:companies,telephone',
                  'mobile'    => 'nullable|max:255|unique:companies,mobile',
                  'website'   => 'nullable|max:255|unique:companies,website',
                ];

            case 'PUT':
            case 'PATCH': // Update
                $company_id = $this->company->id;
                return [
                  'name'      => 'required|max:255',
                  'email'     => "required|max:255|email|unique:companies,email,$company_id",
                  'telephone' => "nullable|max:255|unique:companies,telephone,$company_id",
                  'mobile'    => "nullable|max:255|unique:companies,mobile,$company_id",
                  'website'   => "nullable|max:255|unique:companies,website,$company_id",

                ];
        }
    }

    public function messages()
    {
        return [
          'email.required' => 'Email erforderlich!',
          'name.max'       => 'zuviel Zeichen!',
        ];
    }
}
