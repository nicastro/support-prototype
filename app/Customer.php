<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Customer extends Model
{
    use  UsesTenantConnection;

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
