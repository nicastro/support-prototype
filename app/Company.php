<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Hyn\Tenancy\Traits\UsesTenantConnection;

class Company extends Model
{
    use  UsesTenantConnection;

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }
}
